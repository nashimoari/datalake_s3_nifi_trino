-- A patient's table in mart schema
create table etl_meta.error_log (
    id BIGSERIAL PRIMARY KEY
    ,process_name varchar
    ,process_uuid varchar(50)
    ,error varchar
    ,insert_ts timestamptz NOT NULL DEFAULT NOW()
    );

