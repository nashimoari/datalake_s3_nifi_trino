
create table examples.task1 (event_id int, user_id int, category int, type varchar);


INSERT INTO examples.task1
	SELECT generate_series(1,10000000),
    	floor(random() * 100000 + 1)::int,
    	floor(random() * 1000 + 1)::int,
		case when floor(random() * 20 + 1)::int = 1 then 'click' else 'show' end;


create table examples.task2 (event_id int, user_id int, type varchar, dt timestamptz);

INSERT INTO examples.task2
	SELECT generate_series(1,10000000),
    	floor(random() * 100000 + 1)::int,
		case when floor(random() * 20 + 1)::int = 1 then 'click' else 'show' end,
		timestamp '2022-05-20 00:00:00' +  random() * interval '3 days';



-- sequences
create sequence examples.seq_event_id;
create sequence examples.seq_agg_id;

-- tables
create table examples.user_log (
    event_id int
    ,user_id int
    ,category int
    ,type varchar
    ,dt timestamptz NOT NULL DEFAULT NOW()
    );


create table examples.nomenclature (product_id int, product_name varchar);
create table examples.product_details (product_id int, product_about varchar, product_specification varchar);

create table examples.aggregations(
    agg_id int
    ,dt timestamptz NOT NULL DEFAULT NOW()
    ,aggregation_code varchar
    ,agg_cnt int
    );




-- INSERT INTO examples.nomenclature SELECT generate_series(1,1000000), repeat('X', (random() * 100)::int);
-- INSERT INTO product_details SELECT generate_series(1,1000000), repeat('X', (random() * 100)::int), repeat('X', (random() * 100)::int);
