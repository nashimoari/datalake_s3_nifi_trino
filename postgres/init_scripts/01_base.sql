-- create database for hive metastore
CREATE DATABASE metastore;

-- schema marts : marts for internal consumers
create schema examples;

-- schema etl_meta : ETL process details (logs, errors)
create schema etl_meta;
